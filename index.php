<?php

function httpGet(){
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://document1.apispark.net/v1/sheet1s/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_SSL_VERIFYPEER =>false,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "content-type: application/json",
    "host: document1.apispark.net"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return $response;
}

}
function httpPost($id,$nombre){

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://document1.apispark.net/v1/sheet1s/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_SSL_VERIFYPEER =>false,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"id\":\"".$id."\",\"nombre\":\"".$nombre."\"}",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "content-type: application/json",
    "host: document1.apispark.net"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return $response;
}

}
function httpDel($id){

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://document1.apispark.net/v1/sheet1s/".$id."",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_SSL_VERIFYPEER =>false,
  CURLOPT_CUSTOMREQUEST => "DELETE",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "content-type: application/json",
    "host: document1.apispark.net"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return $response;
}
}
function httpPut($id,$nombre){

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://document1.apispark.net/v1/sheet1s/".$id."",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_SSL_VERIFYPEER =>false,
  CURLOPT_CUSTOMREQUEST => "PUT",
  CURLOPT_POSTFIELDS => "{\"id\":\"".$id."\",\"nombre\":\"".$nombre."\"}",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "content-type: application/json",
    "host: document1.apispark.net"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return $response;
}
}
if(isset($_REQUEST['eliminar'])){
    httpDel($_REQUEST['id']);
}
if(isset($_REQUEST['anadir'])){
    $nom=$_REQUEST['nombre'];
    $id=$_REQUEST['id'];
    httpPost($id,$nom);
}
if(isset($_REQUEST['modificar'])){
    httpPut($_REQUEST['id'],$_REQUEST['nombre']);
}
echo'<style>body{background:lightblue;}td{background:white; border:1px solid black;}</style>';
if(isset($_REQUEST['mostrar'])){
    echo 'Ricard Alcaraz Mancebo';
    $json=httpGet();
    $data = json_decode($json, true);
    echo'<table>';
for($i=0;$i<sizeof($data);$i++){
    echo '<tr>';
    echo '<form action="index.php" method="post"><input type="hidden" name="id" value="'.$data[$i]['id'].'" readonly><td>'.$data[$i]['id'].'</td><td><input name="nombre" value="'.$data[$i]['nombre'].'"></td><th><input value="Modificar" name="modificar" type="submit"/></th> <th><input value="Eliminar" name="eliminar" type="submit"/></th></form>';   
    echo '</tr>';
}
 echo '<tr>';
 echo '<form action="index.php" method="post"><td style="background:grey; border:0px;"></td><td><input name="nombre"></td><th><input value="Añadir" name="anadir" type="submit"/></th></form>';   
  echo '</tr>';
echo'</table>';
}

echo '<form action="index.php" method="post"> <input value="Mostrar Todo" name="mostrar" type="submit"/> </form>';
